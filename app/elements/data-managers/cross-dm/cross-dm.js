{
  class CrossDM extends Polymer.Element {
    static get is() {
      return 'cross-dm';
    }

    static get properties() {
      return {
        name: {
          type: String,
          value: ''
        }
      }
    }

    constructor() {
      super();
    }


    getName() {
      const { name } = this;
      this.dispatchEvent(new CustomEvent('get-name', {
        bubbles: true,
        composed: true,
        detail: {
          name
        }
      }))
    }

    setName(data) {
      this.set('name', data.name);
    }
  }

  customElements.define(CrossDM.is, CrossDM);
}
