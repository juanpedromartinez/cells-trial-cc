{
  class Home extends Polymer.Element {
    constructor() {
      super();
    }

    static get is() {
      return 'home-page';
    }

    loadAction(data) {
      console.log(data);
    }
  }

  customElements.define(Home.is, Home);
}
