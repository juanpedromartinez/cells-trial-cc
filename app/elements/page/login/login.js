{
  class Login extends Polymer.Element {
    constructor() {
      super();
    }

    static get is() {
      return 'login-page';
    }

    loadAction() {
      let name = 'Juan P.';

      this.dispatchEvent(new CustomEvent('set-name', {
        bubbles: true,
        composed: true,
        detail: {
          name
        }
      }));
    }
  }
  customElements.define(Login.is, Login);
}
